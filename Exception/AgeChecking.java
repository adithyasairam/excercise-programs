package Exception;

 class AgeCheckingException extends Exception{
	 AgeCheckingException(String s){
		super(s);
	}
	}
 class AgeChecking{
	public void CheckAge(int age) throws AgeCheckingException {
		if(age<18) {
			throw new AgeCheckingException("wrong Age");
		}
	
		else {
			System.out.println("Eligible");
		}
	}
	public static void main(String[] args) {
try {
	AgeChecking ag=new AgeChecking();
	ag.CheckAge(12);
}

catch (Exception ag){
	System.out.println(ag);
	}

}
}